﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    public float rotVel = 20f;

    public ParticleSystem ps;

    // Update is called once per frame
    void Update()
    {
        transform.RotateAround(transform.position, transform.right, rotVel);// faz a moeda girar
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            ps.Play();// executa a particula
            Destroy(gameObject, 0.5f);
        }
    }
}
