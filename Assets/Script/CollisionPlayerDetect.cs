﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionPlayerDetect : MonoBehaviour
{
    public static bool tocouNoPlayer = false;//variavel q verifica se o player esta no alcance do raio da esfera de colisao


    private void OnTriggerEnter(Collider other) // entrou na esfera
    {
        if (other.CompareTag("Player"))
        {
            tocouNoPlayer = true;
        }
    }

    private void OnTriggerExit(Collider other) //saiu da esfera
    {
        if (other.CompareTag("Player"))
        {
            tocouNoPlayer = false;
        }
    }
}
