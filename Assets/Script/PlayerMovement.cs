﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private Animator anim;

    public GameObject atackColisor;

    private CharacterController charCont;

    public bool detectouArma = false;

    public bool estaArmado = false;

    public Transform arma;

    public Transform mao;

    [SerializeField]
    private float velocidade = 6, velocidadeRotacao = 180;

    float rot = 0f;
    float gravity = 8;

    Vector3 movDir = Vector3.zero;

    void Start()
    {
        anim = GetComponent<Animator>();
        charCont = GetComponent<CharacterController>();
    }

    void Update()
    {

        Movementation();
    
        pegarArma();
        soltarArma();

    }


    private void Movementation()
    {
        if (charCont.isGrounded)
        {

            //movimento e animacao andar e correr
            if (Input.GetKey(KeyCode.W))
            {
                movDir = new Vector3(0, 0, 1);
                
       
                    movDir *= velocidade;
                    movDir = transform.TransformDirection(movDir);
                    anim.SetBool("isWalking", true);
                
            }


            if (Input.GetKey(KeyCode.W) && Input.GetKey(KeyCode.LeftShift))
            {
                movDir = new Vector3(0, 0, 1);
                movDir *= velocidade * 2;
                movDir = transform.TransformDirection(movDir);
                anim.SetBool("isRuning", true);
            }




            if (Input.GetKeyUp(KeyCode.W))
            {
                movDir = new Vector3(0, 0, 0);
                anim.SetBool("isWalking", false);
            }

            if (Input.GetKeyUp(KeyCode.LeftShift))
            {
                anim.SetBool("isRuning", false);
            }
            //////////////////////////////////////////
        }



        //animacao atacar 
        if (estaArmado && Input.GetMouseButton(0) && !(Input.GetKey(KeyCode.W)))//atacking
        {
            anim.SetBool("isAtacking", true);
            StartCoroutine(AguardarParaAtacar(0.25f,true));//esperar 0.25 segundos

        }
        else
        {
            anim.SetBool("isAtacking", false);
            StartCoroutine(AguardarParaAtacar(0.25f, false));//esperar 0.25 segundos

        }

        rot += Input.GetAxis("Horizontal") * velocidadeRotacao * Time.deltaTime;
        transform.eulerAngles = new Vector3(0, rot, 0);

        movDir.y -= gravity * Time.deltaTime;
        charCont.Move(movDir * Time.deltaTime);

    }

    private void pegarArma()
    {
        //rodar animacao

        //StartCoroutine("Aguardar",3f);//esperar 3 segundos

        if (detectouArma && Input.GetKey(KeyCode.E)&&!estaArmado )
        {
            arma.transform.parent = mao.transform.parent;//faz com que a arma vire filha do Osso MAO
            arma.transform.localPosition = (new Vector3(0, 0, 0));//Reseta sua posicao local
            arma.transform.localEulerAngles = new Vector3(78.718f, -156.34f, -252.9987f);//Aplica uma rotacao de forma que fique parecido como se estivesse com uma espada
            estaArmado = true;//fica armado
        }

    }

    private void soltarArma()
    {
        if (estaArmado && Input.GetKey(KeyCode.Q))//se estiver com alguma arme e apertar tecla "Q"
        {
            arma.transform.parent = null; //nao é mais filho do osso MAO
            //abaixo//coloca a arma na posicao do personagem com um acrescimo em y e z //
            arma.transform.position = new Vector3(transform.position.x , 1.15f, transform.position.z * 1.28f);
            arma = null;//Libera o espaco da variavel Arma para receber outra ou a mesma
            detectouArma = false;// nao detecta a arma 
            estaArmado = false;// informa que esta desarmado
        }
    }

    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.CompareTag("arma")&& !estaArmado)
        {
            arma = col.transform;
            detectouArma = true;
        }

    }

    IEnumerator AguardarParaAtacar(float segundos,bool valor)
    {
        
        yield return new WaitForSeconds(segundos);
        atackColisor.SetActive(valor);

    }

}
