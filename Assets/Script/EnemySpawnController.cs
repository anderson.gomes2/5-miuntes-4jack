﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawnController : MonoBehaviour
{
    int maxEnemy = 6; //max de inimigos
    public static int enemyCount = 0; //contador de inimigos
    public Transform[] spawnPoints; //vetor de pontos para spawn
    public GameObject enemy; // prefab do inimigo
    int randomIndex;// indice do vetor

    void criarInimigo()
    {
        randomIndex = Random.Range(0, spawnPoints.Length);//randomiza o indice do vetro de posicoes para o inimigo ser spawnado

        Instantiate(enemy, spawnPoints[randomIndex].position, Quaternion.identity); // instancia o inimigo em uma posicao do vetor

        enemyCount++;//incrementa a qtd de inimigos
    }


    void Awake()
    {

        StartCoroutine(criarEnemies());//inicia a coroutina de criar inimigos
    }

    IEnumerator criarEnemies()
    {
        while(true)
        {
            if (enemyCount < maxEnemy)//garante q so vai ter 6 inimigos no jogo
            {
                
                criarInimigo();
            }
            yield return new WaitForSeconds(2f);//aguarda 2 segundos antes de spawnar outro player

        }
        yield return null;
    }
}
