﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{
    public Transform PlayerPos;

    public Transform[] PatrolPoints;
    public int randomPatrolIndex = 0;

    NavMeshAgent agent;

    public float velocidade = 1f;
    public float velocidadeRotacao = 1f;

    public float life = 100;


    enum Status { patrulha,persegue,ataque}

    [SerializeField]
    Status EnemyStatus;//enumerador para controlar animacoes do player


    private void Awake()
    {
        PatrolPoints[0] = GameObject.Find("enemyPoint-1").transform;
        PatrolPoints[1] = GameObject.Find("enemyPoint-2").transform;
        PatrolPoints[2] = GameObject.Find("enemyPoint-3").transform;
        PatrolPoints[3] = GameObject.Find("enemyPoint-4").transform;
        PatrolPoints[4] = GameObject.Find("enemyPoint-5").transform;
        PatrolPoints[5] = GameObject.Find("enemyPoint-6").transform;
        PatrolPoints[6] = GameObject.Find("enemyPoint-7").transform;
        PatrolPoints[7] = GameObject.Find("enemyPoint-8").transform;

        PlayerPos = GameObject.FindGameObjectWithTag("Player").transform;

        randomPatrolIndex = Random.Range(0, PatrolPoints.Length);
        agent = GetComponent<NavMeshAgent>();
    }

    void patrulhando()//funcao para patrulhar qndo nao encontrar o inimigo
    {
        EnemyStatus = Status.patrulha;

        agent.destination = PatrolPoints[randomPatrolIndex].position;//vai para a posicao do vetor com o indice randomico

        if (Vector3.Distance(transform.position, PatrolPoints[randomPatrolIndex].position)<1.3f)//verifica se chegou proximo da posicao
        {
            randomPatrolIndex = Random.Range(0, PatrolPoints.Length);//randomiza um outro indice do vetor de posicoes

        }

    }

    void perseguindo()//funcao para atacar quando chegar proximo do inimigo
    {
        EnemyStatus = Status.persegue;
        agent.destination = PlayerPos.position;
    }

    void atacando()
    {
        if (Vector3.Distance(transform.position, PlayerPos.position) < 1.6)
        {
            EnemyStatus = Status.ataque;

            //falta implementar dados do ataque.............................................................................
        }
    }//ataque

    // Update is called once per frame
    void Update()
    {
        if (CollisionPlayerDetect.tocouNoPlayer)
        {
            perseguindo();
        }
        else
        {
            patrulhando();
        }
        atacando();

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("atkCol"))//se colidir com o colisor de ataque do player
        {
            EnemySpawnController.enemyCount--;//reduz a variavel enemyCount do script EnemySpawnController
            Destroy(gameObject,0.3f);
        }
    }

}
